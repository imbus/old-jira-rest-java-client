Build using maven v3+

In `~/.m2/settings.xml` you need a few settings to publish to artifactory.
The basic settings you can generate in artifactory by logging in and selecting `Maven Settings` on the left.
You want to set `meta-maven` for everything (releases and snapshots)
The generated Settings you need to edit a bit:

Add the atlassian repos:

```
#!xml
<repository>
  <id>atlassian-public</id>
  <url>https://m2proxy.atlassian.com/repository/public</url>
  <snapshots>
    <enabled>false</enabled>
    <updatePolicy>daily</updatePolicy>
    <checksumPolicy>warn</checksumPolicy>
  </snapshots>
  <releases>
    <enabled>false</enabled>
    <checksumPolicy>warn</checksumPolicy>
  </releases>
</repository>
```

Add atlassian plugin repos:

```
#!xml
<pluginRepository>
  <id>atlassian-public</id>
  <url>https://m2proxy.atlassian.com/repository/public</url>
  <releases>
    <enabled>true</enabled>
    <checksumPolicy>warn</checksumPolicy>
  </releases>
  <snapshots>
    <checksumPolicy>warn</checksumPolicy>
  </snapshots>
</pluginRepository>
```

Also it is required to add the password to the `server` declarations inside a `<password>` element to be able to publish.